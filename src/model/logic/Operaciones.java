package model.logic;

import java.util.ArrayList;
import java.util.Stack;

import model.data_structures.MiQueue;
import model.data_structures.MiStack;

public class Operaciones {

	
	public boolean expresionBienFormada(String expre)
	{
		MiStack stack;    
		String cadena=expre;
		for (int i = 0 ; i < cadena.length() ; i++)
		{
			if ((cadena.charAt(i) == '(') || (cadena.charAt(i) == '[')) 
			{
				stack.push(cadena.charAt(i));
			} 
			else 
			{
				if (cadena.charAt(i)==')') 
				{
					if (stack.pop()!= '(') 
					{
						return false;
					}
				} 
				else 
				{
					if (cadena.charAt(i)==']') 
					{
						if (stack.pop()!= '[')
						{
							return false;
						}
					} 

				}
			}   
		}
		if (stack.isEmpty()) 
		{
			return true;
		} 
		else 
		{
			return false;
		}

		return false;
	}
	
	public <T> ArrayList ordenarPila(String expre)
	{
		ArrayList lista1 = new ArrayList();
		ArrayList listaOperadores = new ArrayList();
		ArrayList listaNumeros = new ArrayList();
		ArrayList listaFinal = new ArrayList();
		MiQueue<T> lista = new MiQueue();
		for (int i =0; i < expre.length(); i++)
		{
			char valor = expre.pop();
			if ((valor == '(')|| (valor ==']')|| (valor==')')||(valor=='['))
			{
				lista1.add(valor);
			}
			else if ((valor=='+')||(valor=='*')||(valor=='/')||(valor=='-'))
			{
				listaOperadores.add(valor);
			}
			else
			{
				listaNumeros.add(valor);
			}
		}
		listaFinal.add(lista1);
		listaFinal.add(listaOperadores);
		listaFinal.add(listaNumeros);
		
		for (int i=0;i<listaFinal.size();i++)
		{
			lista.enqueue(listaFinal.get(i));
		}
		return listaFinal;
	}


}
