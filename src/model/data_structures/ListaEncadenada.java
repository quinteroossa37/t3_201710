package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaEncadenada.Iterador;

public class ListaEncadenada <T> implements ILista {
	
	private Nodo<T> actual;
	private Nodo<T> primero;
	private int posicion;
	
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterador();
	}
	public class Iterador implements Iterator{

		@Override
		public boolean hasNext() 
		{
			// TODO Auto-generated method stub
			if(primero.getSiguiente()!=null)
			{
				return true;
			}
			return false;
		}

		@Override
		public Nodo<T> next() 
		{
			// TODO Auto-generated method stub

			return actual.getSiguiente();
		}

	}

	public void cambiarPrimerNodo()
	{
		primero = primero.getSiguiente();
	}
	
	public void eliminarUltimo()
	{
		Nodo nodo = darNodoPosicion(this.darNumeroElementos() - 1) ;
		nodo = null;
	}
	
	@Override
	public T darElemento(int pos) 
	{
		// TODO Auto-generated method stub
		int contador = 0;
		actual = primero;
		while(contador<=pos){
			actual = actual.getSiguiente();
			contador++;
		}
		posicion = pos;

		return actual.getNodo();
	}
	
	public Nodo<T> darNodoPosicion(int pos) {
		// TODO Auto-generated method stub
		int contador = 0;
		actual = primero;
		while(contador<=pos){
			actual = actual.getSiguiente();
			contador++;
		}
		posicion = contador;

		return actual;
	}


	@Override
	public int darNumeroElementos() 
	{
		// TODO Auto-generated method stub
		Nodo<T> nodo = new Nodo<T>();
		nodo = primero;
		int contador = 0;
		while(nodo.getSiguiente().getNodo()!=null){
			contador++;
		}
		return contador;
	}

	@Override
	public T darElementoPosicionActual() 
	{
		// TODO Auto-generated method stub
		return actual.getNodo();
	}

	@Override
	public boolean avanzarSiguientePosicion()
	{
		// TODO Auto-generated method stub
		if(actual.getSiguiente().getNodo()!=null){
			actual = actual.getSiguiente();
			posicion ++;
			return true;
		}
		return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(darNodoPosicion((posicion)-1)!=null){
			actual = darNodoPosicion((posicion)-1);
			return true;
		}
		return false;
	}
	@Override
	public void agregarElementoFinal(Object elem) {
		// TODO Auto-generated method stub
		actual = primero;
		posicion = 0;
		if( primero.getNodo().equals(null)){
			primero.setNodo(elem);

			return;
		}
		while(!actual.getSiguiente().getNodo().equals(null)){
			posicion++;
		}
		actual.getSiguiente().setNodo(elem);

	}
	

}
