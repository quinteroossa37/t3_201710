package model.data_structures;

public class MiQueue<T> implements IQueue<T>{

	private int size;
	private ListaEncadenada<T> lista;
	
	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if (size==0)
		{
			return true;
		}
		return false;
	}



	@Override
	public T dequeue() {
		// TODO Auto-generated method stub
		Nodo nodo= (Nodo<T>) lista.darElemento(0);
		lista.cambiarPrimerNodo();
		return (T)nodo;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void enqueue(Object item) {
		// TODO Auto-generated method stub
		lista.agregarElementoFinal((T)item);
		size++;
	}

}
