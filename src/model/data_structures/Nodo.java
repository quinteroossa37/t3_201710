package model.data_structures;

public class Nodo <T>{

	private T item;
	private Nodo<T> siguiente;

	public Nodo()
	{

	}

	public T getNodo()
	{
		return item;
	}


	public Nodo<T> getSiguiente()
	{
		return siguiente;
	}

	public  void setNodo(Object elem)
	{
		item = (T) elem;
	}


	public void setSiguiente(Nodo<T> nSiguiente)
	{
		siguiente= nSiguiente;
	}

}


