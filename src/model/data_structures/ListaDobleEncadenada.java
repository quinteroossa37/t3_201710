package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub

		return new ClaseIterador();
	}

	private int index = 0;
	private NodoDoble<T> ante= new NodoDoble<T>();
	private NodoDoble<T> sig = new NodoDoble<T>();
	private NodoDoble<T> actual = new NodoDoble <T>();

	private class ClaseIterador implements Iterator <T>
	{

		public boolean hasNext() {
			// TODO Auto-generated method stub
			if (ante.getSiguiente()!= null)
			{
				return true;
			}
			else
			{
				return false;
			}

		}

		@Override
		public T next() {
			// TODO Auto-generated method stub

			return actual.getSiguiente();
		}

		public T previous()
		{
			return actual.getAnterior();
		}

		@Override
		public void remove() 
		{
			// TODO Auto-generated method stub
			sig = (NodoDoble<T>) actual.getSiguiente();
			ante= (NodoDoble<T>) actual.getAnterior();
			if (sig == null)
			{
				ante.setSiguiente(null);
			}
			else
			{
				ante.setSiguiente((T) sig);
			}

			sig.setAnterior((T)ante);

		}

	}

	@Override
	public void agregarElementoFinal(T elem) 
	{
		// TODO Auto-generated method stub
		actual = ante;
		while (actual.getSiguiente()!=null)
		{
			if (actual.getSiguiente()==null)
			{
				actual.setSiguiente(elem);
			}
			else 
			{
				actual = (NodoDoble<T>) actual.getSiguiente();
			}
		}
	}

	@Override
	public T darElemento(int pos) 
	{
		// TODO Auto-generated method stub
		int i = 0;
		actual = ante;
		while (i<pos)
		{
			actual = (NodoDoble<T>) actual.getSiguiente();
			i++;
			if (i == pos)
			{
				return actual.getNodo();
			}
		}
		return null;

	}




	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		int i = 0;
		actual = ante;
		while (actual.getSiguiente()!=null)
		{
			i++;
			actual = (NodoDoble<T>) actual.getSiguiente();
		}
		return i;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub	
		return actual.getNodo();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(( ((NodoDoble<T>) actual.getSiguiente()).getNodo())!=null){
			actual = (NodoDoble<T>) actual.getSiguiente();
			index ++;
			return true;
		}

		return false;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(( ((NodoDoble<T>) actual.getAnterior()).getNodo())!=null){
			actual = (NodoDoble<T>) actual.getAnterior();
			index --;
			return true;
		}
		return false;
	}

}
