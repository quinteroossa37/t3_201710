package model.data_structures;

public class NodoDoble <t>
{
	private t item;
	private t anterior ;
	private t siguiente;
	
	public NodoDoble()
	{
		
	}
	
	public t getNodo()
	{
		return item;
	}
	
	public  t getAnterior()
	{
		return anterior;
	}
	
	public t getSiguiente()
	{
		return siguiente;
	}
	
	public  void setNodo(t nNodo)
	{
		item = nNodo;
	}
	
	public void setAnterior(t nAnterior)
	{
		anterior = nAnterior;
	}
	
	public void setSiguiente(t nSiguiente)
	{
		siguiente= nSiguiente;
	}
	
	
	
}
