package model.data_structures;

public class MiStack<T> implements IStack{
	private int size =0;
	private ListaEncadenada<T> lista ;
	
	@Override
	public boolean isEmpty()
	{
		// TODO Auto-generated method stub
		if (lista.darNumeroElementos()==0)
		{
			return true;
		}
		return false;
	}
	
	@Override
	public Nodo pop() 
	{
		// TODO Auto-generated method stub
		//return lista.darElemento(lista.darNumeroElementos());
		Nodo elem = (Nodo) lista.darElemento(lista.darNumeroElementos()-1);
		lista.eliminarUltimo();
		return elem;
	}
	
	@Override
	public void push(Object item) {
		lista.agregarElementoFinal((T) item);
		size++;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}
	
	
}
