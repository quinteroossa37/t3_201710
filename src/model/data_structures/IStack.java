package model.data_structures;

public interface IStack <T>{

	//Si la pila esta vac�a
	public boolean isEmpty();
	
	//Toma el primer objeto de la pila y lo retorna 
	public T pop();
	
	//Agrega un elemento a la pila
	public void push (T item);
	
	//Retorna el primer elemento de la pila sin quitarlo 
	public int size();
}
