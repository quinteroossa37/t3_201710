package model.data_structures;

public interface IQueue <T>{

	public boolean isEmpty();
	public void enqueue(T item);
	public T dequeue();
	public int size();
	
}
