package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.Controller;



public class View {

	private static void printMenu()
	{
		System.out.println("1. Digitar expresión");
		System.out.println("2. Expresión bien formada");
		System.out.println("3. Ordenar expresión");
		System.out.println("4. Exit");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
	}
	
	private static String readData()
	{
		String data ;
		
		data = new Scanner(System.in).nextLine();

		return data;
	}

	public static void main(String[] args)
	{
		
		//IntegersBag bag = null;
		Scanner sc = new Scanner(System.in);
		
		for(;;){
		  printMenu();
		  
		 
		  int option = sc.nextInt();
		  switch(option)
		  {
			  case 1:  String expre = readData(); System.out.println("--------- \n Datos tomados  \n---------");
			  break;
			  
			  case 2: System.out.println("--------- \n "+Controller.expresionBienFormada(expre)+" \n---------");
			  break;
			  
			  case 3: System.out.println("--------- \n La expresión ordenada es  "+Controller.ordenarPila(expre)+" \n---------");		  
			  break;
			 
			  case 4: System.out.println("Bye!!  \n---------"); sc.close(); return;		  
			 
			  
			  default: System.out.println("--------- \n Invalid option !! \n---------");
			  
		  }
		  
		
	}
	
}

}
