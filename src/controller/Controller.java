package controller;

import java.util.ArrayList;

import model.data_structures.MiStack;
import model.logic.Operaciones;

public class Controller {

	private static Operaciones model = new Operaciones();
	
	public static String expresionBienFormada(String expre)
	{
		
		if ( model.expresionBienFormada(expre))
		{
			return "La expresión se encuentra bien formada";
		}
		else
		{
			return "La expresión no se encuentra bien formada";
		}
	}
	
	public static ArrayList ordenarPila(String expre)
	{
		return model.ordenarPila(expre);
	}
	
	
}
